<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventSpeakers extends Model
{
    public function EventDate()
    {
        return $this->hasOne('App\EventDate', 'id','event_date');
    }
    public function Person()
    {
        return $this->hasOne('App\Person', 'id','person');
    }
}
