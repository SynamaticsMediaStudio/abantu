<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
public function parent(){
    return $this->belongsTo('App\Interest','parent')->where('parent',0)->with('parent');
}

public function children()
{
    return $this->hasMany('App\Interest','parent')->with('children');
}
public function Collects()
{
    return $this->hasMany('App\PersonInterest','interest','id');
}
}
