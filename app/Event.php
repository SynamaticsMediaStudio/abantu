<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public function Organizer()
    {
        return $this->hasOne('App\Organization', 'id','organization');
    }
    public function Dates()
    {
        return $this->hasMany('App\EventDate', 'event','id');
    }
    public function Tags()
    {
        $tag = "";
        foreach (explode(",",$this->tags) as $item) {
            $tag .= "<span class=\"badge badge-success\">$item</span>";
        }
        return $tag;
    }
}
