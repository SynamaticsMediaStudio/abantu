<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonInterest extends Model
{
    public function Interest()
    {
        return $this->belongsTo("App\Interest","interest","id");
    }
    public function Person()
    {
        return $this->belongsTo("App\Person","person","id");
    }
}
