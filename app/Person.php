<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $fillable = ['name','email','mobile','gender','city','state','country','tags','position','organization','type'];
    
    public function Organization()
    {
        return $this->hasOne("App\Organization","id","organization");
    }
    public function Events()
    {
        return $this->hasMany('App\EventAttendance', 'person','id');
    }
    public function Category()
    {
        return $this->hasOne("App\PersonType","id","type");
    }
    public function Skills()
    {
        return $this->hasMany("App\PersonSkill","person","id");
    }
    public function Interests()
    {
        return $this->hasMany("App\PersonInterest","person","id");
    }
    public function CategoryName()
    {
        if($this->Category){
            return $this->Category->category;
        }
        else{
            return "Category not Assigned";
        }
    }
}
