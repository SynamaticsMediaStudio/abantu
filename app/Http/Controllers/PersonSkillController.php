<?php

namespace App\Http\Controllers;

use App\PersonSkill;
use Illuminate\Http\Request;

class PersonSkillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PersonSkill  $personSkill
     * @return \Illuminate\Http\Response
     */
    public function show(PersonSkill $personSkill)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PersonSkill  $personSkill
     * @return \Illuminate\Http\Response
     */
    public function edit(PersonSkill $personSkill)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PersonSkill  $personSkill
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PersonSkill $personSkill)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PersonSkill  $personSkill
     * @return \Illuminate\Http\Response
     */
    public function destroy(PersonSkill $personSkill)
    {
        //
    }
}
