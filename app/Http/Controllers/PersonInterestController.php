<?php

namespace App\Http\Controllers;

use App\PersonInterest;
use Illuminate\Http\Request;

class PersonInterestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PersonInterest  $personInterest
     * @return \Illuminate\Http\Response
     */
    public function show(PersonInterest $personInterest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PersonInterest  $personInterest
     * @return \Illuminate\Http\Response
     */
    public function edit(PersonInterest $personInterest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PersonInterest  $personInterest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PersonInterest $personInterest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PersonInterest  $personInterest
     * @return \Illuminate\Http\Response
     */
    public function destroy(PersonInterest $personInterest)
    {
        //
    }
}
