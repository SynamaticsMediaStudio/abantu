<?php

namespace App\Http\Controllers;

use App\tags;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  \App\tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function index(tags $tags)
    {
        $tags   = $tags->paginate(10);
        return view('tags.index',['tags'=>$tags]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function show(tags $tags,$tag)
    {
        $tags   =  $tags->find($tag);
        $events = \App\Event::where("tags","LIKE","%$tags->tag%")->get();
        $people = \App\Person::where("tags","LIKE","%$tags->tag%")->get();
        return view('tags.show',['tag'=>$tags,"events"=>$events,"people"=>$people]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function edit(tags $tags)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tags $tags)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function destroy(tags $tags)
    {
        //
    }
}
