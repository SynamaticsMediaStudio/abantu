<?php

namespace App\Http\Controllers;

use App\PersonType;
use Illuminate\Http\Request;

class PersonTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("people.categories.index",['categories'=>PersonType::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("people.categories.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'category'=>"required"
        ]);
        $new = new PersonType;
        $new->category = $request->category;
        $new->save();
        return redirect()->back()->with(['success'=>"$new->category Added"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PersonType  $personType
     * @return \Illuminate\Http\Response
     */
    public function show(PersonType $personType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PersonType  $personType
     * @return \Illuminate\Http\Response
     */
    public function edit(PersonType $personType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PersonType  $personType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PersonType $personType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PersonType  $personType
     * @return \Illuminate\Http\Response
     */
    public function destroy(PersonType $personType)
    {
        //
    }
}
