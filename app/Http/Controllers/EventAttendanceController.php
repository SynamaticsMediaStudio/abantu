<?php

namespace App\Http\Controllers;

use App\EventDate;
use App\EventAttendance;
use App\Person;
use App\tags;
use Illuminate\Http\Request;
use Importer;

class EventAttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->person as $key) {
            $atten = new EventAttendance;
            $atten->person       = $key; 
            $atten->event_date   = $request->event;
            $atten->status       = '1';
            $atten->save();
       }
       return redirect()->back()->with(['success'=>"Added Registration"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventAttendance  $eventAttendance
     * @return \Illuminate\Http\Response
     */
    public function show(EventAttendance $eventAttendance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventAttendance  $eventAttendance
     * @return \Illuminate\Http\Response
     */
    public function edit(EventAttendance $eventAttendance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventAttendance  $eventAttendance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $eventAttendance      =   EventAttendance::findorfail($id);
        $event      =   $eventAttendance->EventDate->Event;
        $tags   =   explode(",",$event->tags);
        $person =   $eventAttendance->Person;
        $prs    =   explode(",",$person->tags);
        $eventAttendance = EventAttendance::findorfail($id);
        $eventAttendance->status = $request->status;
        $eventAttendance->save();
        switch ($eventAttendance->status) {
            case 2:
                foreach ($tags as $tag) {
                    if(!in_array($tag,$prs)){
                        $prs[] = $tag;
                    }
                }
                $person->tags = implode(",",$prs);
                $person->save();
                break;
            
            default:
                
                break;
        }


        return redirect()->back()->with(['success'=>"Updated"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventAttendance  $eventAttendance
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $eventAttendance = EventAttendance::findorfail($id);
        $eventAttendance->delete();
        return redirect()->back()->with(['success'=>"Deleted"]);
    }
   public function import(Request $request)
    {
        $file   = $request->file('upload');
        $fl     = $file->move('storage/temp',$file->getClientOriginalName());
        $excel = Importer::make('Csv');
        $excel->load($fl);
        $collection = $excel->getCollection();
        $fields     = $collection[0];
        $data       = [];
        for ($i=1; $i <count($collection) ; $i++) {
            $data[$i] = [];
            for ($s=0; $s <count($fields) ; $s++) {
                $data[$i][$fields[$s]] = $collection[$i][$s];
            }
        }
        foreach($data as $dat){
            $person = Person::where('email',$dat['person'])->first();
            if($person){
                $attcheck = EventAttendance::where(["person"=>$person->id,"event_date"=>$request->event])->first();
                if(!$attcheck){
                    $event = new EventAttendance;
                    $event->event_date  = $request->event;
                    $event->person      = $person->id;
                    switch ($dat['status']) {
                        case 'Registered':
                            $event->status = 1;
                            break;
                        case 'Attended':
                            $event->status = 2;
                            break;
                        case 'Cancelled':
                            $event->status = 3;
                            break;
                        
                        default:
                            $event->status = 1;
                        break;
                    }
                    $event->person      = $person->id;
                    $event->save();
                    $tags   =   explode(",",$event->EventDate->Event->tags);
                    $prs    =   explode(",",$person->tags);
                    switch ($event->status) {
                        case 2:
                            foreach ($tags as $tag) {
                                if(!in_array($tag,$prs)){
                                    $prs[] = $tag;
                                }
                            }
                            $person->tags = implode(",",$prs);
                            $person->save();
                            break;
                        default:
                            break;
                    }
                }                
                
            }
        }
        unlink($fl);
        return redirect()->back()->with('success', 'Upload and Import Sucess!');
    }
}
