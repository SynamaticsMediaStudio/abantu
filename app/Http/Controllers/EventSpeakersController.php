<?php

namespace App\Http\Controllers;

use App\EventSpeakers;
use App\tags;
use Illuminate\Http\Request;

class EventSpeakersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       foreach ($request->speaker as $key) {
            $atten = new EventSpeakers;
            $atten->person       = $key; 
            $atten->event_date   = $request->event;
            $atten->save();
       }
       return redirect()->back()->with(['success'=>"Added Speaker"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventSpeakers  $eventSpeakers
     * @return \Illuminate\Http\Response
     */
    public function show(EventSpeakers $eventSpeakers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventSpeakers  $eventSpeakers
     * @return \Illuminate\Http\Response
     */
    public function edit(EventSpeakers $eventSpeakers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventSpeakers  $eventSpeakers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventSpeakers $eventSpeakers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventSpeakers  $eventSpeakers
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $eventSpeakers = EventSpeakers::findorfail($id);
        $eventSpeakers->delete();
       return redirect()->back()->with(['success'=>"Deleted Speaker"]);
    }
}
