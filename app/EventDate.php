<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class EventDate extends Model
{
    public function Event()
    {
        return $this->hasOne('App\Event', 'id','event');
    }
    public function Attendes()
    {
        return $this->hasMany('App\EventAttendance', 'event_date','id');
    }
    public function Speakers()
    {
        return $this->hasMany('App\EventSpeakers', 'event_date','id');
    }
    public function isDate()
    {
        if (Carbon::parse($this->date_from)->diffInDays($this->date_to) == 0){
            return Carbon::parse($this->date_from)->toFormattedDateString();
        }
        else{
            return Carbon::parse($this->date_from)->toFormattedDateString() ."to". Carbon::parse($this->date_to)->toFormattedDateString();
        }
    }
    public function From()
    {
        return Carbon::parse($this->date_from)->toFormattedDateString();
    }
    public function To()
    {
        return Carbon::parse($this->date_to)->toFormattedDateString();
    }
    public function FromData()
    {
        return Carbon::parse($this->date_from)->format('d-m-Y');
    }
    public function ToData()
    {
        return Carbon::parse($this->date_to)->format('d-m-Y');
    }
}
