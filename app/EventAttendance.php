<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventAttendance extends Model
{
    public function EventDate()
    {
        return $this->hasOne('App\EventDate', 'id','event_date');
    }
    public function Person()
    {
        return $this->hasOne('App\Person', 'id','person');
    }
    public function Status()
    {
        
        switch($this->status){

            case 1:
                $status = "<span class=\"text-secondary\">Registered</span>";
            break;
            case 2:
                $status = "<span class=\"text-success\">Attended</span>";
            break;
            case 3:
                $status = "<span class=\"text-danger\">Cancelled</span>";
            break;
            default:
                $status = "<span class=\"text-secondary\">Status not available</span>";
            break;
        }
        return $status;
    }
    public function StatusText()
    {
        return strip_tags($this->Status());
    }
}
