<?php

namespace App\Imports;

use App\Organization;
use Maatwebsite\Excel\Concerns\ToModel;

class OrganizationImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
            return new Organization([
                'name'     => $row[0],
                'location' => $row[1], 
                'type'   => $row[2], 
                'tags'   => $row[3], 
           ]);
    }
}
