<form action="{{route('events.update',[$event->id])}}" method="POST" class="row">
    @csrf
    <input type="hidden" name="_method" value="PUT">
    <div class="card-body row">
        <div class="form-group col-sm-12">
            <label for="org_name">Name</label>
            <input type="text" name="name" id="org_name" class="form-control" value="@if(old('name')) {{old('name')}} @else {{$event->name}} @endif">
        </div>
        <div class="form-group col-sm-12 col-md-6">
            <label for="org_tags">Tags</label>
            <input type="text" name="tags" id="org_tags" class="form-control typeahead" value="@if(old('tags')) {{old('tags')}} @else {{$event->tags}} @endif">
        </div>
        <div class="form-group col-sm-12 col-md-6">
            <label for="organization">Organizer</label>
            <select name="organization" id="organization" class="form-control selectjs-basic-single" required>
                @forelse ($organizations as $organization)
                    <option @if(old('organization') || $event->organization == $organization->id) selected @endif value="{{$organization->id}}">{{$organization->name}}</option>
                @empty
                <option selected disabled >No Organizations available.</option>
                @endforelse
            </select>
        </div>
    </div>
    <div class="form-group col-sm-12 col-md-12">
        <button type="submit" class="btn btn-outline-success btn-sm float-right">Save</button>
    </div>
</form>
