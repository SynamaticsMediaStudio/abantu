@extends('layouts.internal')
@section('title',"Events")
@section('content')
<div class="page-header">
    <h3 class="mb-2">Events <i>({{$events->count()}})</i></h3>
    <div class="page-breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}" class="breadcrumb-link">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Events</a></li>
            </ol>
        </nav>
    </div>
 </div>

 <div class="container">
     <div class="row">
         <div class="col-sm-12 col-md-10 ">
            <div class="card table-responsive">
                <table class="table table-sm">
                    <thead class="table-secondary">
                        <tr>
                            <th>Name</th>
                            <th>Organizer</th>
                            <th>Tags</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($events as $event)
                        <tr>
                            <td><a href="{{route('events.show',$event->id)}}">{{$event->name}}</a></td>
                            <td><a href="{{route('organization.show',$event->Organizer->id)}}">{{$event->Organizer->name}}</a></td>
                            <td>{!!$event->Tags()!!}</td>
                        </tr>
                        @empty
                        <tr><th colspan="4" class="text-center">No Events</th></tr>
                        @endforelse
                    </tbody>
                </table>
                <div class="py-1"></div>
                <div class="card-footer">
                    {{$events->links()}}
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-2">
            <div class="card card-body">
                 <a href="{{route('events.create')}}" class="btn btn-sm btn-outline-secondary">Create New</a> 
                 <br>
                 <a href="#event-upload-modal" data-toggle="modal" data-target="#event-upload-modal" class="btn btn-sm btn-outline-primary">Upload CSV</a>
            </div>
        </div>
     </div>
 </div>
    <form action="{{route('event.import')}}" method="POST" class="modal" id="event-upload-modal" enctype="multipart/form-data">
        <div class="modal-dialog modal-dialog-centered modal-sm">
            <div class="modal-content">
                <div class="modal-body">
                    @csrf
                <div class="form-group">
                    <input type="file" accept=".csv" required  name="upload" class="form-control">
                </div>
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                <button class="btn btn-info btn-sm" type="submit">Upload</button>
            </div>
        </div>
    </form>

@endsection

