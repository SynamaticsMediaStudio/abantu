@extends('layouts.internal')
@section('title',"Add new Event")
@section('content')
<div class="container">
    <div class="py-4"></div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form class="card" action="{{route('events.store')}}" method="POST">
                @csrf
                <div class="card-header">Create events
                    <div class="float-right">
                        <a href="{{route('events.index')}}" class="btn btn-sm btn-outline-secondary">Back to List</a>
                    </div>
                </div>
                <div class="card-body row">
                    <div class="form-group col-sm-12">
                        <label for="org_name">Name</label>
                        <input type="text" name="name" id="org_name" class="form-control" value="@if(old('name')) {{old('name')}} @endif">
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="org_tags">Tags</label>
                        <input type="text" name="tags" id="org_tags" class="form-control typeahead" value="@if(old('tags')) {{old('tags')}} @endif">
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="organization">Organizer</label>
                        <select name="organization" id="organization" class="form-control selectjs-basic-single" required>
                            @forelse ($organizations as $organization)
                                <option @if(old('organization') == $organization->id) selected @endif value="{{$organization->id}}">{{$organization->name}}</option>
                            @empty
                                <option selected disabled >No Organizations available.</option>
                            @endforelse
                        </select>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-outline-success btn-sm float-right">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
