<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/typeahead.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito|Material+Icons" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/base.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (session('success'))
    <div class="alert alert-success">
        <p>{{session('success')}}</p>
    </div>
@endif

        <nav class="navbar navbar-expand-md navbar-light py-0 navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" id="search" type="search" placeholder="Search" aria-label="Search">
                <a class="btn btn-outline-success my-2 my-sm-0" href="{{route('search')}}">Search</a>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                @if (Route::has('register'))
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                @endif
                            </li>
                        @else
                            <li class="nav-item"><a class="nav-link" href="{{ route('home') }}">{{ __('Dashboard') }}</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('interests.index') }}">{{ __('Interests') }}</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('skills.index') }}">{{ __('Skills') }}</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('staff.index') }}">{{ __('Staff') }}</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('people.index') }}">{{ __('People') }}</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('organization.index') }}">{{ __('Organizations') }}</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('events.index') }}">{{ __('Events') }}</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('contact-categories.index') }}">{{ __('Contact Categories') }}</a></li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
<script>
$(document).ready(function() {
    $('select').each(function(){
        $(this).select2();
    })
    var states = [];
    @foreach(App\tags::all() as $tags)
        states.push("{{$tags->tag}}");
    @endforeach
    var states = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.whitespace,
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: states
    });
    states.initialize();
    $('#org_tags').tagsinput({
        typeaheadjs: {
            source: states.ttAdapter()
        }
    });
});
$(function(){
    var applicationsearch = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  remote: {
    url: '/api/search/%QUERY',
    wildcard: '%QUERY'
  }
});

$('#search').typeahead(null, {
  name: 'applicationsearch',
  display: 'name',
  source: applicationsearch,
  templates: {
    empty: [
      '<div class="empty-message">',
        'No results.',
      '</div>'
    ].join('\n'),
    suggestion: function(data) {
    return '<p><strong>' + data.content + '</p>';
}
  }
});
$('#search').on('typeahead:selected', function(evt, item) {
    window.location.href = item.link;
})
})
</script>


</body>
</html>
