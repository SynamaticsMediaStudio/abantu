<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{asset("assets/vendor/bootstrap/css/bootstrap.min.css")}}">
    <link href="{{asset("assets/vendor/fonts/circular-std/style.css")}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset("assets/libs/css/style.css")}}">
    <link rel="stylesheet" href="{{asset("assets/vendor/fonts/fontawesome/css/fontawesome-all.css")}}">
    <link rel="stylesheet" href="{{asset("assets/vendor/vector-map/jqvmap.css")}}">
    <link href="{{asset("assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css")}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset("assets/vendor/charts/chartist-bundle/chartist.css")}}">
    <link rel="stylesheet" href="{{asset("assets/vendor/charts/c3charts/c3.css")}}">
    <link rel="stylesheet" href="{{asset("assets/vendor/charts/morris-bundle/morris.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("assets/vendor/daterangepicker/daterangepicker.css")}}" />
    <link rel="stylesheet" href="{{asset("css/select2.min.css")}}">
    <link rel="stylesheet" href="{{asset("assets/snackbar.min.css")}}">
    <link rel="stylesheet" href="{{asset("css/bootstrap-tagsinput.css")}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset("assets/vendor/bootstrap-colorpicker/%40claviska/jquery-minicolors/jquery.minicolors.css")}}">
    <link rel="stylesheet" href="{{asset("css/base.css")}}">
    <link href="https://fonts.googleapis.com/css?family=Nunito|Material+Icons" rel="stylesheet" type="text/css">
    <title> @yield('title') | {{ __('settings.int_organization_name') }}</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.29.2/dist/sweetalert2.min.css">
    <script src="{{asset("assets/snackbar.min.js")}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.29.2/dist/sweetalert2.all.min.js"></script>
    <!-- jquery 3.3.1  -->
    <script src="{{asset("assets/vendor/jquery/jquery-3.3.1.min.js")}}"></script>
    <!-- bootstap bundle js -->
    <script src="{{asset("assets/vendor/bootstrap/js/bootstrap.bundle.js")}}"></script>
    <!-- slimscroll js -->
    <script src="{{asset("assets/vendor/slimscroll/jquery.slimscroll.js")}}"></script>

    <!-- main js -->
    <script src="{{asset("assets/libs/js/main-js.js")}}"></script>
    <!-- gauge js -->
    <script src="{{asset("assets/vendor/gauge/gauge.min.js")}}"></script>
   <!-- daterangepicker js -->
    <script src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="{{ asset('js/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/typeahead.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script src="{{ asset('assets/vendor/bootstrap-colorpicker/jquery-asColor/dist/jquery-asColor.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-colorpicker/jquery-asGradient/dist/jquery-asGradient.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-colorpicker/jquery-asColorPicker/dist/jquery-asColorPicker.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-colorpicker/%40claviska/jquery-minicolors/jquery.minicolors.min.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <script>
    $(document).ready(function() {
        $('.table').DataTable();
    });
    </script>
<style>
.sidebar-dark.nav-left-sidebar .navbar-nav .nav-link{
    color:#ffffff;
}
.navbar-brand{
    font-weight: 100;
    color: {{ __('settings.int_theme_color') }}
}
.sidebar-dark.nav-left-sidebar .navbar-nav .nav-link:focus,
.sidebar-dark.nav-left-sidebar .navbar-nav .nav-link.active,
.sidebar-dark.nav-left-sidebar .navbar-nav .nav-link:hover{
    background-color: #ffffff;
    color: {{ __('settings.int_theme_color') }};
}
</style>
</head>
<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg bg-white fixed-top py-0">
                <a class="navbar-brand" href="/">{{ __('settings.int_organization_name') }}</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse " id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto navbar-right-top">
                        <li class="nav-item">
                            <div id="custom-search" class="top-search-bar">
                                <input class="form-control" type="text" id="search" placeholder="Search..">
                            </div>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('search') }}">{{ __('Search') }}</a></li>
                        <li class="nav-item dropdown nav-user">
                            <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="assets/images/avatar-1.jpg" alt="" class="user-avatar-md rounded-circle"></a>
                            <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                                <div class="nav-user-info">
                                    <h5 class="mb-0 text-white nav-user-name">{{ Auth::user()->name }} </h5>
                                </div>
                                <a class="dropdown-item" href="{{ route('settings.index') }}"><i class="fas fa-cog mr-2"></i>Setting</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                        @csrf
                                <button class="dropdown-item" type="submit"><i class="fas fa-power-off mr-2"></i>Logout</button>
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        @guest
        @else
        <div class="nav-left-sidebar sidebar-dark" style="background-color:{{ __('settings.int_theme_color') }}">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            @forelse (__('settings.menu_side_bar') as $item)
                                <li class="nav-item"><a class="@if(\Request::route()->getName() == $item['route']) active @endif nav-link" href="{{ route($item['route']) }}">{{$item['title']}}</a></li>
                            @empty
                                
                            @endforelse
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        @endguest
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-finance">
                <div class="container-fluid dashboard-content">
                    @yield('content')
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            Copyright © 2018 {{ App\Settings::find('1')->first()->option_value }}. All rights reserved</a>. 
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="text-md-right footer-links d-none d-sm-block">
                                <a href="javascript: void(0);">About</a>
                                <a href="javascript: void(0);">Support</a>
                                <a href="javascript: void(0);">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <script>
    $(function() {
        $('input[name="daterange"]').daterangepicker({
            opens: 'left'
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });
    });
    </script>
<script>
$(document).ready(function() {
    $('select').each(function(){
        $(this).select2();
    })
    var states = [];
    @foreach(App\tags::all() as $tags)
        states.push("{{$tags->tag}}");
    @endforeach
    var states = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.whitespace,
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: states
    });
    states.initialize();
    $('#org_tags, .tags-item').tagsinput({
        typeaheadjs: {
            source: states.ttAdapter()
        }
    });
});
$(function(){
    var applicationsearch = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  remote: {
    url: '/api/search/%QUERY',
    wildcard: '%QUERY'
  }
});

$('#search').typeahead(null, {
  name: 'applicationsearch',
  display: 'name',
  source: applicationsearch,
  templates: {
    empty: [
      '<div class="empty-message">',
        'No results.',
      '</div>'
    ].join('\n'),
    suggestion: function(data) {
    return '<p><strong>' + data.content + '</p>';
}
  }
});
$('#search').on('typeahead:selected', function(evt, item) {
    window.location.href = item.link;
})
});
$(function(){

    @if ($errors->any())
    @foreach ($errors->all() as $error)
    Snackbar.show({ showAction: false,text:"{{$error}}"});
    @endforeach
    @endif
    @if (session('success'))
        Snackbar.show({ showAction: false,text:"{{session('success')}}"});
    @endif
})
</script>
</body>
</html>