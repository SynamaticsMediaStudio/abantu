@extends('layouts.internal')
@section('title',$organization->name)
@section('content')
<div class="page-header">
   <h3 class="mb-2">{{$organization->name}}</small></h3>
   <div class="page-breadcrumb">
       <nav aria-label="breadcrumb">
           <ol class="breadcrumb">
               <li class="breadcrumb-item"><a href="{{route('home')}}" class="breadcrumb-link">Dashboard</a></li>
               <li class="breadcrumb-item"><a href="{{route('organization.index')}}" class="breadcrumb-link">Organizations</a></li>
               <li class="breadcrumb-item"><a href="" class="breadcrumb-link">{{$organization->name}}</a></li>
           </ol>
       </nav>
   </div>
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                @csrf
                <div class="card-header"> {{$organization->name}}
                    <div class="float-right">
                        <a href="{{route('organization.index')}}" class="btn btn-sm btn-outline-secondary">Back to List</a>
                        <a href="{{route('organization.edit',$organization->id)}}" class="btn btn-sm btn-outline-primary">Edit</a>
                    </div>
                </div>
                <div class="">
                    <table class="table">
                        <tr>
                            <th>Name</th>
                            <td>{{$organization->name}}</td>
                        </tr>
                        <tr>
                            <th>Category</th>
                            <td>{{$organization->type}}</td>
                        </tr>
                        <tr>
                            <th>Location</th>
                            <td>{{$organization->location}}</td>
                        </tr>
                        <tr>
                            <th>Point of Contact</th>
                            <td>
                                @if ($organization->Person)
                                    {{$organization->Person->name}}
                                @else
                                No Contact attached
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{{$organization->email}}</td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td>{{$organization->phone}}</td>
                        </tr>
                        <tr>
                            <th>Tags</th>
                            <td>
                                @forelse (explode(",",$organization->tags) as $item)
                                    <span class="badge badge-success">{{$item}}</span>
                                @empty
                                    
                                @endforelse
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="card">
                <div class="card-header">People in {{$organization->name}} </div>
                <div class="">
                    <table class="table table-sm">
                        <thead class="table-secondary">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Position</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($organization->Employees as $employee)
                                <tr>
                                    <th><a href="{{route('people.show',$employee->id)}}">{{$employee->name}}</a></th>
                                    <td>{{$employee->email}}</td>
                                    <td>{{$employee->mobile}}</td>
                                    <td>{{$employee->Category->category}} ({{$employee->position}})</td>
                                </tr>
                            @empty
                                <tr>
                                    <th colspan="4" class="text-center">No people attached to this organization</th>
                                </tr>
                            @endforelse
                    </table>
                </div>
            </div>
            <div class="card">
                <div class="card-header">Events By {{$organization->name}} </div>
                <div class="">
                    <table class="table table-sm">
                        <thead class="table-secondary">
                            <tr>
                                <th>Events</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($organization->Events as $event)
                                <tr>
                                    <th><a href="{{route('events.show',$event->id)}}">{{$event->name}}</a></th>
                                </tr>
                            @empty
                                <tr>
                                    <th colspan="4" class="text-center">No people attached to this organization</th>
                                </tr>
                            @endforelse
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
