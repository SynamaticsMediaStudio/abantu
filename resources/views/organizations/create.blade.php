@extends('layouts.internal')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form class="card" action="{{route('organization.store')}}" method="POST">
                @csrf
                <div class="card-header">Create Organization
                    <div class="float-right">
                        <a href="{{route('organization.index')}}" class="btn btn-sm btn-outline-secondary">Back to List</a>
                    </div>
                </div>
                <div class="card-body row">
                    <div class="form-group col-sm-12">
                        <label for="org_name">Name</label>
                        <input type="text" name="name" id="org_name" class="form-control" required value="@if(old('name')) {{old('name')}} @endif">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="org_location">Location</label>
                        <input type="text" name="location" id="org_location" class="form-control" value="@if(old('location')) {{old('location')}} @endif">
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="org_category">Category</label>
                        <select name="category" id="org_category" class="form-control selectjs-basic-single">
                            <option @if(old('category') == 'School') selected @endif value="School">School</option>
                            <option @if(old('category') == 'College') selected @endif value="College">College</option>
                            <option @if(old('category') == 'Institution') selected @endif value="Institution">Institution</option>
                            <option @if(old('category') == 'Non-Profit') selected @endif value="Non-Profit">Non-Profit</option>
                            <option @if(old('category') == 'Company') selected @endif value="Company">Company</option>
                            <option @if(old('category') == 'Startup') selected @endif value="Startup">Startup</option>
                            <option @if(old('category') == 'Goverment') selected @endif value="Goverment">Goverment</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="org_person">Contact Endpoint</label>
                        <select name="person" id="org_person" class="form-control">
                            <option value="" selected>No Organization</option>
                            @foreach ($people as $person)
                                <option @if(old('person') == $person->id) selected @endif value="$person->id">{{$person->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="org_email">Email</label>
                        <input type="email" name="email" id="org_email" class="form-control" value="{{old('email')}}">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="org_phone">Phone</label>
                        <input type="text" name="phone" id="org_phone" class="form-control" value="{{old('phone')}}">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="org_tags">Tags</label>
                        <input type="text" name="tags" id="org_tags" class="form-control typeahead" value="{{old('tags')}}">
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-outline-success btn-sm float-right">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
