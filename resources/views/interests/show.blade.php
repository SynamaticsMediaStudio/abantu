@extends('layouts.internal')

@section('content')
<div class="page-header">
    <a href="{{route('interests.create')}}" class="btn btn-outline-info btn-sm float-right">Add Interest</a>
   <h3 class="mb-2">
       @if ($interest->parent)
       {{$interest->Parent->interest}} -
       @endif
       {{$interest->interest}}
    </h3>
   <div class="page-breadcrumb">
       <nav aria-label="breadcrumb">
           <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}" class="breadcrumb-link">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('people.index')}}" class="breadcrumb-link">Interests</a></li>
                @if ($interest->parent)
                <li class="breadcrumb-item"><a href="{{route('interests.show',[$interest->Parent->id])}}" class="breadcrumb-link">{{$interest->Parent->interest}}</a></li>
                @endif
                <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">{{$interest->interest}}</a></li>
           </ol>
       </nav>
   </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-4 card">
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($interest->Collects as $item)
                        <tr>
                            <th><a href="{{route('people.show',[$item->Person->id])}}">{{$item->Person->name}}</a></th>
                        </tr>
                    @empty
                        
                    @endforelse
                    @if ($interest->parent !== 0)
                        @forelse ($interest->children as $child)
                            @forelse ($child->Collects as $item)
                                <tr>
                                    <th><a href="{{route('people.show',[$item->Person->id])}}">{{$item->Person->name}}</a></th>
                                </tr>
                            @empty
                            @endforelse
                        @empty
                            
                        @endforelse
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection