@extends('layouts.internal')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form action="{{route('interests.store')}}" method="POST" class="card">
                @csrf
                <div class="card-header">
                    <a href="{{route('interests.index')}}" class="btn btn-outline-secondary btn-sm float-right">Cancel</a>
                    Add interest
                </div>
                <div class="card-body">
                   <div class="form-group">
                       <label for="interest">Title</label>
                       <input type="text" id="interest" name="interest" class="form-control">
                   </div>
                   <div class="form-group">
                       <label for="parent">Parent</label>
                       <select id="parent" name="parent" class="form-control selectjs-basic-single">
                           <option value="0">No Parent</option>
                           @forelse ($interests as $interest)
                               <option value="{{$interest->id}}">{{$interest->interest}}</option>
                           @empty
                               
                           @endforelse
                       </select>
                   </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-outline-success btn-sm float-right">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection