@extends('layouts.internal')

@section('content')
<div class="page-header">
    <a href="{{route('interests.create')}}" class="btn btn-outline-info btn-sm float-right">Add Interest</a>
   <h3 class="mb-2">Interests</h3>
   <div class="page-breadcrumb">
       <nav aria-label="breadcrumb">
           <ol class="breadcrumb">
               <li class="breadcrumb-item"><a href="{{route('home')}}" class="breadcrumb-link">Dashboard</a></li>
               <li class="breadcrumb-item"><a href="{{route('people.index')}}" class="breadcrumb-link">Interests</a></li>
           </ol>
       </nav>
   </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-6 card">
            <table class="table sm">
                <thead>
                    <tr>
                        <th>Interest</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($interests as $interest)
                    <tr>
                        <td><a href="{{route('interests.show',$interest->id)}}">{{$interest->interest}}</a></td>
                        <td>
                        <form action="{{route('interests.destroy',$interest->id)}}" method="POST">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="btn btn-sm btn-danger"><i class="material-icons">delete</i></button>
                        </form>
                    </td>
                    </tr>
                    @foreach ($interest->children as $child)
                    <tr>
                        <td><a href="{{route('interests.show',$child->id)}}">{{$interest->interest}} -> {{$child->interest}}</a></td>
                        <td>
                        <form action="{{route('interests.destroy',$child->id)}}" method="POST">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="btn btn-sm btn-danger"><i class="material-icons">delete</i></button>
                        </form>
                    </td>
                    </tr>
                    @endforeach
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection