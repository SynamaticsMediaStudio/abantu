@extends('layouts.internal')
@section('title',"People")
@section('content')
<div class="page-header">
   <h3 class="mb-2">People <i>({{$people->count()}})</i></h3>
   <div class="page-breadcrumb">
       <nav aria-label="breadcrumb">
           <ol class="breadcrumb">
               <li class="breadcrumb-item"><a href="{{route('home')}}" class="breadcrumb-link">Dashboard</a></li>
               <li class="breadcrumb-item"><a href="{{route('people.index')}}" class="breadcrumb-link">People</a></li>
           </ol>
       </nav>
   </div>
</div>
<div class="container">
    <div class="row" id="people-section">
        <div class="col-sm-12 col-md-10">
            <div class="card table-responsive card-body">
                <table class="table table-sm">
                    <thead class="table-secondary">
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Category</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($people as $person)
                            <tr>
                                <th><a href="{{route('people.show',$person->id)}}">{{$person->name}}</a></th>
                                <td>{{$person->email}}</td>
                                <td>{{$person->mobile}}</td>
                                <td>{{$person->CategoryName()}}</td>
                            </tr>
                        @empty
                            <tr>
                                <th colspan="4" class="text-center">No contacts</th>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-12 col-md-2">
            <div class="card card-body">
                <a href="{{route('people.create')}}" class="btn btn-sm btn-outline-secondary">Create New</a>
                <br>
                <a href="#people-upload-modal" data-toggle="modal" data-target="#people-upload-modal" class="btn btn-sm btn-outline-primary">Upload CSV</a>
                </div>
            </div>
        </div>
    </div>
    <form action="{{route('people.import')}}" method="POST" class="modal" id="people-upload-modal" enctype="multipart/form-data">
        <div class="modal-dialog modal-dialog-centered modal-sm">
            <div class="modal-content">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <input type="file" accept=".csv" required  name="upload" class="form-control">
                    </div>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-info btn-sm" type="submit">Upload</button>
            </div>
        </div>
    </form>


@endsection
