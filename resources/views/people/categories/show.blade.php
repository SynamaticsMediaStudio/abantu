@extends('layouts.internal')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @csrf
                <div class="card-header"> {{$organization->name}}
                    <div class="float-right">
                        <a href="{{route('organization.index')}}" class="btn btn-sm btn-outline-secondary">Back to List</a>
                        <a href="{{route('organization.edit',$organization->id)}}" class="btn btn-sm btn-outline-primary">Edit</a>
                    </div>
                </div>
                <div class="card-body">
                    <strong>About {{$organization->name}} </strong>
                    <table class="table">
                        <tr>
                            <th>Name</th>
                            <td>{{$organization->name}}</td>
                        </tr>
                        <tr>
                            <th>Category</th>
                            <td>{{$organization->type}}</td>
                        </tr>
                        <tr>
                            <th>Location</th>
                            <td>{{$organization->location}}</td>
                        </tr>
                        <tr>
                            <th>Tags</th>
                            <td>
                                @forelse (explode(",",$organization->tags) as $item)
                                    <span class="badge badge-success">{{$item}}</span>
                                @empty
                                    
                                @endforelse
                            </td>
                        </tr>
                    </table>
                    <strong>People in {{$organization->name}} </strong>
                    <table class="table table-sm">
                        <thead class="table-secondary">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Position</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($organization->Employees as $employee)
                                <tr>
                                    <th><a href="{{route('people.show',$employee->id)}}">{{$employee->name}}</a></th>
                                    <td>{{$employee->email}}</td>
                                    <td>{{$employee->mobile}}</td>
                                    <td>{{$employee->profession_type}} ({{$employee->profession}})</td>
                                </tr>
                            @empty
                                <tr>
                                    <th colspan="4" class="text-center">No people attached to this organization</th>
                                </tr>
                            @endforelse
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
