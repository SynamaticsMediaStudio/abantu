@extends('layouts.internal')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form class="card" action="{{route('contact-categories.store')}}" method="POST">
                @csrf
                <div class="card-header">Create Contact Category
                    <div class="float-right">
                        <a href="{{route('contact-categories.index')}}" class="btn btn-sm btn-outline-secondary">Back to List</a>
                    </div>
                </div>
                <div class="card-body row">
                    <div class="form-group col-sm-12">
                        <label for="org_category">Name</label>
                        <input type="text" name="category" id="org_category" class="form-control" required value="@if(old('category')) {{old('category')}} @endif">
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-outline-success btn-sm float-right">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
