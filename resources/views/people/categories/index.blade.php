@extends('layouts.internal')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">People Categories
                    <a href="{{route('contact-categories.create')}}" class="btn btn-sm btn-outline-secondary float-right">Create New</a>
                </div>
                <table class="table table-sm">
                    <thead class="table-secondary">
                        <tr>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($categories as $category)
                            <tr>
                                <th><a href="{{route('contact-categories.show',$category->id)}}">{{$category->category}}</a></th>
                                {{-- <th>{{$category->category}}</th> --}}
                            </tr>
                        @empty
                            <tr>
                                <th colspan="4" class="text-center">No categories</th>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
