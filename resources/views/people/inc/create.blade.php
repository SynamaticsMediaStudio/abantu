                        <div class="form-group col-sm-12">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="name" class="form-control" value="@if(old('name')) {{old('name')}} @endif">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="email">Email ID</label>
                            <input type="email" name="email" id="email" class="form-control" value="@if(old('email')) {{old('email')}} @endif">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="mobile">Mobile No</label>
                            <input type="text" name="mobile" id="mobile" class="form-control" value="@if(old('mobile')) {{old('mobile')}} @endif">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="gender">Gender</label>
                            <select name="gender" id="gender" class="form-control">
                                <option @if(old('gender') == "Male") selected @endif value="Male">Male</option>
                                <option @if(old('gender') == "Female") selected @endif value="Female">Female</option>
                                <option @if(old('gender') == "Trans") selected @endif value="Trans">Trans</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="dob">Date of Birth</label>
                            <input type="text" name="dob" id="dob" data-provide="datepicker" autocomplete="off" class="form-control" value="@if(old('dob')) {{old('dob')}} @endif">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="city">City</label>
                            <input type="text" name="city" id="city" class="form-control" value="@if(old('city')) {{old('city')}} @endif">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="state">State</label>
                            <input type="text" name="state" id="state" class="form-control" value="@if(old('state')) {{old('state')}} @endif">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="country">Country</label>
                            <input type="text" name="country" id="country" class="form-control" value="@if(old('country')) {{old('country')}} @else India @endif">
                        </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="org_tags">Tags</label>
                        <input type="text" name="tags" id="org_tags" class="form-control typeahead" value="@if(old('tags')) {{old('tags')}} @endif">
                    </div>
                        <div class="form-group col-sm-12">
                            <strong>Professional Details</strong>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="position">Profession</label>
                            <input type="text" name="position" id="position" class="form-control" value="@if(old('position')) {{old('position')}} @endif">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="organization">Organization</label>
                            <select name="organization" id="organization" class="form-control selectjs-basic-single" required>
                                @forelse ($organizations as $organization)
                                    <option @if(old('organization') == $organization->id) selected @endif value="{{$organization->id}}">{{$organization->name}}</option>
                                @empty
                                    <option selected disabled >No Organizations available.</option>
                                @endforelse
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="skills">Skills</label>
                            <select name="skills[]" multiple id="skills" class="form-control">
                                @foreach ($skills as $skill)
                                    <optgroup label="{{$skill->skill}}">
                                        @foreach ($skill->children as $child)
                                            <option @if(old('skills') == $child->id) selected @endif value="{{$child->id}}">{{$child->skill}}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="interests">Interests</label>
                            <select name="interests[]" multiple id="interests" class="form-control">
                                @foreach ($interests as $interest)
                                    <optgroup label="{{$interest->interest}}">
                                        @foreach ($interest->children as $child)
                                            <option @if(old('interests') == $child->id) selected @endif value="{{$child->id}}">{{$child->interest}}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="profession_type">Category</label>
                            @foreach ($types as $type)
                                <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="radio" name="profession_type" id="profession_type_{{$type->id}}" value="{{$type->id}}" @if(old('profession_type') == "{{$type->category}}") checked @endif >
                                <label class="custom-control-label" for="profession_type_{{$type->id}}">
                                    {{$type->category}}
                                </label>
                                </div>                                
                            @endforeach
                        </div>
