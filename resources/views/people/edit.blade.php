@extends('layouts.internal')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form action="{{route('people.update',[$person->id])}}" class="card" method="POST">
                <input type="hidden" name="_method" value="PUT">
                @csrf
                <div class="card-header">Edit Contact
                    <div class="float-right">
                        <a href="{{route('people.index')}}" class="btn btn-sm btn-outline-secondary">Back to List</a>
                        <button type="submit" class="btn btn-outline-success btn-sm">Save</button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="name" class="form-control" value="@if(old('name')) {{old('name')}} @else {{$person->name}} @endif">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="email">Email ID</label>
                            <input type="email" name="email" id="email" class="form-control" value="@if(old('email')) {{old('email')}} @else {{$person->email}} @endif">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="mobile">Mobile No</label>
                            <input type="text" name="mobile" id="mobile" class="form-control" value="@if(old('mobile')) {{old('mobile')}} @else {{$person->mobile}} @endif">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="gender">Gender</label>
                            <select name="gender" id="gender" class="form-control">
                                <option @if(old('gender') == "Male" || $person->gender == "Male") selected @endif value="Male">Male</option>
                                <option @if(old('gender') == "Female" || $person->gender == "Female") selected @endif value="Female">Female</option>
                                <option @if(old('gender') == "Trans" || $person->gender == "Trans") selected @endif value="Trans">Trans</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="dob">Date of Birth</label>
                            <input type="text" name="dob" id="dob" data-provide="datepicker" autocomplete="off" class="form-control" value="@if(old('dob')) {{old('dob')}} @else {{$person->dob}}  @endif">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="city">City</label>
                            <input type="text" name="city" id="city" class="form-control" value="@if(old('city')) {{old('city')}} @else {{$person->city}}  @endif">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="state">State</label>
                            <input type="text" name="state" id="state" class="form-control" value="@if(old('state')) {{old('state')}} @else {{$person->city}}  @endif">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="country">Country</label>
                            <input type="text" name="country" id="country" class="form-control" value="@if(old('country')) {{old('country')}} @else {{$person->country}} @endif">
                        </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="org_tags">Tags</label>
                        <input type="text" name="tags" id="org_tags" class="form-control typeahead" value="@if(old('tags')) {{old('tags')}} @else {{$person->tags}} @endif">
                    </div>
                        <div class="form-group col-sm-12">
                            <strong>Professional Details</strong>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="position">Profession</label>
                            <input type="text" name="position" id="position" class="form-control" value="@if(old('position')) {{old('position')}} @else {{$person->position}} @endif">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="organization">Organization</label>
                            <select name="organization" id="organization" class="form-control selectjs-basic-single" required>
                                @forelse ($organizations as $organization)
                                    <option @if(old('organization') == $organization->id || $person->organization == $organization->id) selected @endif value="{{$organization->id}}">{{$organization->name}}</option>
                                @empty
                                    <option selected disabled >No Organizations available.</option>
                                @endforelse
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="skills">Skills</label>
                            <select name="skills[]" multiple id="skills" class="form-control">
                                @foreach ($skills as $skill)
                                    <optgroup label="{{$skill->skill}}">
                                        @foreach ($skill->children as $child)
                                            <option @if(old('skills') == $child->id) selected @endif value="{{$child->id}}"
                                            @foreach ($person->Skills as $pskill)
                                                @if ($pskill->Skill->id == $child->id)
                                                selected
                                                @endif
                                            @endforeach
                                            >{{$child->skill}}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="interests">Interests</label>
                            <select name="interests[]" multiple id="interests" class="form-control">
                                @foreach ($interests as $interest)
                                    <optgroup label="{{$interest->interest}}">
                                        @foreach ($interest->children as $child)
                                            <option @if(old('interests') == $child->id) selected @endif value="{{$child->id}}"
                                            @foreach ($person->Interests as $pinterest)
                                                @if ($pinterest->Interest->id == $child->id)
                                                selected
                                                @endif
                                            @endforeach
                                            >{{$child->interest}}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="profession_type">Category</label>
                            @foreach ($types as $type)
                                <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="radio" name="profession_type" id="profession_type_{{$type->id}}" value="{{$type->id}}" @if(old('profession_type') == $type->id || $person->type == $type->id) checked @endif >
                                <label class="custom-control-label" for="profession_type_{{$type->id}}">{{$type->category}}</label>
                                </div>                                
                            @endforeach
                        </div>
                        <div class="form-group col-sm-12">
                            <input type="hidden" name="data" id="data" class="form-control" value="@if(old('data')) {{old('data')}} @endif">
                        </div>
                        <div class="form-group col-sm-12">
                            <a href="javascript:void()" class="add-data btn btn-outline-dark btn-sm float-right">Add</a>
                        </div>
                        <div class="form-group col-sm-12" id="fields">
                        @if($person->data)                    
                            @forelse (json_decode($person->data) as $title => $name)
                            <div class="jsonser form-group">
                                <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">{{$title}}</span>
                                </div>
                                <input type="text" data-prac-id="{{str_replace(' ', '', $title)}}" class="form-control main-input" value="{{$title}}">
                                <input type="text" data-prac-id="{{str_replace(' ', '', $title)}}" id="xpred-{{str_replace(' ', '', $title)}}" class="form-control" value="{{$name}}">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-danger remove_field" type="button" id="button-addon_{{str_replace(' ', '', $title)}}">Remove</button>
                                </div>
                                </div>
                            </div>
                            @empty
                                
                            @endforelse
                        @endif
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-outline-success btn-sm float-right">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
$(document).ready(function(e) {
    var max_fields      = 100; //maximum input boxes allowed
    var wrapper         = $("#fields"); //Fields wrapper
    var add_button      = $(".add-data"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            var Inpoe = `
            <div class="jsonser form-group">
                <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Additional Information</span>
                </div>
                <input type="text" data-prac-id="`+x+`" class="form-control main-input">
                <input type="text" data-prac-id="`+x+`" id="xpred-`+x+`" class="form-control">
                <div class="input-group-append">
                    <button class="btn btn-outline-danger remove_field" type="button" id="button-addon_`+x+`">Remove</button>
                </div>
                </div>
            </div>
            `;
            $(wrapper).append(Inpoe); //add input box
        }
    });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parents('.jsonser').remove(); x--;
        UpdateJson()
    })
$(document).on("keyup",".jsonser input",function(){
     UpdateJson()
})

});

function UpdateJson(){
    var FLS = $('.jsonser .main-input');
    var jsp = {};
    FLS.each(function(data){
       var Cif = $("#xpred-"+$(this).data('prac-id')).val()
       var Cid = $(this).val();
        jsp[Cid] = Cif;
    })
    $("[name='data']").val(JSON.stringify(jsp))
}
</script>
@endsection
