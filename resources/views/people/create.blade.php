@extends('layouts.internal')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form action="{{route('people.store')}}" class="card" method="POST">
                @csrf
                <div class="card-header">Add Contact
                    <div class="float-right">
                        <a href="{{route('people.index')}}" class="btn btn-sm btn-outline-secondary">Back to List</a>
                        <button type="submit" class="btn btn-outline-success btn-sm">Save</button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        @include('people.inc.create')
                        <div class="form-group col-sm-12">
                            <input type="hidden" name="data" id="data" class="form-control" value="@if(old('data')) {{old('data')}} @endif">
                        </div>
                        <div class="form-group col-sm-12">
                            <a href="javascript:void()" class="add-data btn btn-outline-dark btn-sm float-right">Add</a>
                        </div>
                        <div class="form-group col-sm-12" id="fields">
                        @if(old('data'))    
                            @forelse (json_decode(old('data')) as $title => $name)
                            <div class="jsonser form-group">
                                <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">{{$title}}</span>
                                </div>
                                <input type="text" data-prac-id="{{str_replace(' ', '', $title)}}" class="form-control main-input" value="{{$title}}">
                                <input type="text" data-prac-id="{{str_replace(' ', '', $title)}}" id="xpred-{{str_replace(' ', '', $title)}}" class="form-control" value="{{$name}}">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-danger remove_field" type="button" id="button-addon_{{str_replace(' ', '', $title)}}">Remove</button>
                                </div>
                                </div>
                            </div>
                            @empty
                                
                            @endforelse
                        @endif
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-outline-success btn-sm float-right">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
$(document).ready(function(e) {
    var max_fields      = 100; //maximum input boxes allowed
    var wrapper         = $("#fields"); //Fields wrapper
    var add_button      = $(".add-data"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            var Inpoe = `
            <div class="jsonser form-group">
                <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Additional Information</span>
                </div>
                <input type="text" data-prac-id="`+x+`" class="form-control main-input">
                <input type="text" data-prac-id="`+x+`" id="xpred-`+x+`" class="form-control">
                <div class="input-group-append">
                    <button class="btn btn-outline-danger remove_field" type="button" id="button-addon_`+x+`">Remove</button>
                </div>
                </div>
            </div>
            `;
            $(wrapper).append(Inpoe); //add input box
        }
    });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parents('.jsonser').remove(); x--;
        UpdateJson()
    })
$(document).on("keyup",".jsonser input",function(){
     UpdateJson()
})

});

function UpdateJson(){
    var FLS = $('.jsonser .main-input');
    var jsp = {};
    FLS.each(function(data){
       var Cif = $("#xpred-"+$(this).data('prac-id')).val()
       var Cid = $(this).val();
        jsp[Cid] = Cif;
    })
    $("[name='data']").val(JSON.stringify(jsp))
}
</script>
@endsection
